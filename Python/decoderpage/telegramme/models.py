from django.db import models
from django.urls import reverse

# Create your models here.

class Telegramme(models.Model):
    raw = models.CharField(max_length=255)
    freq = models.CharField(max_length=255)
    epoch = models.FloatField()
    timestamp = models.CharField(max_length=255)
    protocol = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    encoded = models.BooleanField()
    nomsg = models.BooleanField()
    geo = models.BooleanField()
    address = models.CharField(max_length=255)
    function = models.CharField(max_length=255)
    msg = models.CharField(max_length=255)
    lat = models.FloatField()
    lon = models.FloatField()
    ident = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    fromm = models.CharField(max_length=255)
    too = models.CharField(max_length=255)
    via = models.CharField(max_length=255)
    path = models.CharField(max_length=255)
    format = models.CharField(max_length=255)
    telemetry_message = models.CharField(max_length=255)
    comment = models.CharField(max_length=255)
    text = models.CharField(max_length=255)


    def __str__(self):
        return self.raw
    
    def get_absolute_url(self):
        return reverse('telegramme-detail', args=[str(self.id)])
