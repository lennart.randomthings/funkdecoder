import OSMPythonTools.nominatim

def searchaddressOSM(address):
    nominatim = OSMPythonTools.nominatim.Nominatim()
    address = nominatim.query(address)
    address = address.toJSON()
    return address