import configparser
import logging
import sys
import os

config = configparser.ConfigParser()
configDir = "./config"
configFile = "config/config.ini"

"""
Helper function to get the value of a config parameter

conf = helper.config.initconfig()
"""

def initconfig():
    if not os.path.isdir(configDir):
        os.mkdir(configDir)
    if not os.path.isfile(configFile):
        create()
    #Konfigdatei initialisieren
    try:
        #Config Datei auslesen
        config.read(configFile)
        return config['DEFAULT']
    except Exception as e:
        print("Error while loading the Config file:" + \
            str(sys.exc_info()) + "\n" + \
            str(e.message) + " " + str(e.args))
        logging.error("Error while loading the Config file" + str(sys.exc_info()) + \
            str(sys.exc_info()) + "\n" + \
            str(e.message) + " " + str(e.args))

def create():
    with open("config/config.template.ini", "r") as configtemp:
        config = configtemp.read()
    with open("config/config.ini", "w") as configfile:
        configfile.write(config)

def check():
    conf = initconfig()
    checklist =['freq',
                'ppm_error',
                'squelch',
                'fms',
                'poc512',
                'poc1200',
                'poc2400',
                'aprs',
                'zvei',
                'device',
                'gain',
                'network_address',
                'telegramplugin',
                'telegramtoken',
                'telegramchatid',
                'emailplugin',
                'mailhost',
                'mailport',
                'mailuser',
                'mailpassword',
                'toaddrs',
                'aprsplugin',
                'aprsCALL',
                'aprspasscode',
                'doubleFilter',
                'doubleFiltertime',
                'filternetwork',
                'filterencoded',
                'filternomsg',
                'filtermsglen',
                ]
    for i in checklist:
        if i not in conf:
            print("Error: Config file is missing a value for " + i)
            logging.error("Error: Config file is missing a value for " + i)
            sys.exit()
