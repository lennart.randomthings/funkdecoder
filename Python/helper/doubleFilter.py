#!/usr/bin/python
# -*- coding: utf-8 -*-


import logging
import time
import helper.config

conf = helper.config.initconfig()

doubleList = []

def checkID(decoded):
    timestamp = int(time.time()) # Get Timestamp

    logging.debug("checkID: (%s)", decoded['msg'])
    for (xTimestamp, xMsg, address) in doubleList:
        # given ID found?
        # return False if the first entry in double_ignore_time is found, we will not check for younger ones...
        if decoded['msg'].strip() == xMsg and \
        decoded['address'] == address and \
        timestamp < xTimestamp + int(conf['doubleFiltertime']):
            logging.info("%s double alarm (id): %s within %s second(s)", decoded['protocol'], decoded['msg'], timestamp-xTimestamp)
            return False
    newEntry(decoded)
    return True


def newEntry(decoded):
    global doubleList
    timestamp = int(time.time()) # Get Timestamp
    if 'address' in decoded:
        doubleList.append((timestamp, decoded['msg'].strip(), decoded['address']))

        logging.debug("Added %s to doubleList", decoded['msg'].strip())
        # now check if list has more than n entries:
        if len(doubleList) > 10:
            # we have to kill the oldest one
            doubleList.pop(0)
